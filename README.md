# P3.1 → V6 Povezovanje na podatkovno bazo

V pripravah na vaje **V6** bomo **implementirali nakupovalno košarico** in **pripravili e-Račun** ter spoznali naslednje koncepte:

* globalne spremenljivke vs. seja,
* nakupovanje pesmi iz seznama 100 najbolje prodajanih s pomočjo nakupovalne košarice,
* izpis e-Računa po standardu [e-SLOG](https://e-slog.gzs.si/) iz nakupovalne košarice v seji.